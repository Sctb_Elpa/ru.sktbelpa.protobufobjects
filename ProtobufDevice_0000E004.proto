syntax = "proto2";

package ru.sktbelpa.r4_2_prodm;


enum INFO {
    // верися этого протокола
    PROTOCOL_VERSION = 2;

    // идентификатор устройства РЧ2 productomer
    R4_2_PRODUCTOMER_ID = 0xE004;

    // ИД, преднозначенный для поиска
    ID_DISCOVER = 0xFFFF;

    MAGICK = 0x09;
}

//------------------------------------------------------------------------------

// Общий запрос: Вызывающий всегда посылает только этот запрос, который содержит
// все возможные данные как необязательные поля.
message Request {
    // Идентификатор запроса. Служит для определения пары запрос/ответ
    required uint32             		id = 1;
    // id устройства (идентификация)
    required uint32             		deviceID = 2;
    // версия протокола запрашивающего
    required uint32             		protocolVersion = 3;
    
    // Таймштамп клиента. Если он есть в сообщении, то часы устройства будут установлены в соответствии с ним
    optional uint64            			setClock = 4;
    // Установка/Запрос настроек
    optional WriteSettingsReq   		writeSettingsReq = 5;
    // Запрс результатов измерений
    optional GetMeasureResultsReq       getMeasureResultsReq = 6;
    // Запрос истории измеренй
    optional HistoryReq					getHistory = 7;
    
    // Запрос статистики по памяти устройства
    optional bool						getMeminfo = 20;
}

//------------------------------------------------------------------------------

// Общий ответ: Устройство всегда отвечает только этим ответом, который содержит
// все возможные данные как необязательные поля.
message Response {
    // Идентификатор запроса, которому соответствует этот ответ
    required uint32             		id = 1;
    // id устройства (идентификация)
    required uint32             		deviceID = 2;
    // версия протокола
    required uint32             		protocolVersion = 3;
    // общий статус выполнения
    required STATUS             		Global_status = 4;
    // таймштамп устройства, когда фактически был сгененрирован ответ
    required uint64            			timestamp = 5;
            
    // Все настройки, если таковые были запрошены
    optional SettingsResponse   		settings = 20;
    // Ответ с результатами измерений
    optional GetMeasureResultsResponce  getMeasureResultsResponce = 21;
    
    // История измерений
    optional History					history = 30;
    
    // Статистика по памяти
    optional Mallinfo					mallinfo = 40;
}

//------------------------------------------------------------------------------

// код выполнения операции
enum STATUS {
    // успешное завершение
    OK = 0;
    // недопустимое значение поля
    ERRORS_IN_SUBCOMMANDS = 1;

    PROTOCOL_ERROR = 100;
}

enum CHANEL_STATUS {
	CHANEL_OK = 0;
	NO_SIGNAL = 1;
	TUNING = 2;
}

//------------------------------------------------------------------------------

message WriteSettingsReq {
	// серийный номер
	optional uint32 				Serial = 1;
	// Время измерения по каналу 1
	optional uint32 				Chanel1MesureTime_ms = 2;
	// Время измерения по каналу 2
	optional uint32 				Chanel2MesureTime_ms = 3;
	// Опорная частота
	optional uint32 				Fref = 4;
	
	// enable
	optional bool					Chanel1Enabled = 10;
	optional bool					Chanel2Enabled = 11;
	
	// display freq display
	optional bool					Chanel1ShowFreq = 20;
	optional bool					Chanel2ShowFreq = 21;
	
	// Коэффициенты
	optional TCoefficientsSet		Chanel1Coefficients = 30;
	optional TCoefficientsSet		Chanel2Coefficients = 31;
}

message GetMeasureResultsReq {
	required bool					Chanel1result = 1;
	required bool					Chanel2result = 2;
}

//-----------------------------------------------------------------------------

message SettingsResponse {
	// серийный номер
	required uint32 				Serial = 1;
	// Время измерения по каналу 1
	required uint32 				Chanel1MesureTime_ms = 2;
	// Время измерения по каналу 2
	required uint32 				Chanel2MesureTime_ms = 3;
	// Опорная частота
	required uint32 				Fref = 4;
	
	// enable
	required bool					Chanel1Enabled = 10;
	required bool					Chanel2Enabled = 11;
	
	// display freq display
	required bool					Chanel1ShowFreq = 20;
	required bool					Chanel2ShowFreq = 21;
	
	// Коэффициенты
	required TCoefficientsGet		Chanel1Coefficients = 30;
	required TCoefficientsGet		Chanel2Coefficients = 31;
}

message GetMeasureResultsResponce {
	repeated MeasureResult			result = 1;					
}

// ----------------------------------------------------------------------------

message TCoefficientsGet {
	required float 					T0 = 1;
	required float					C0 = 2;
	required float					C1 = 3;
	required float					C2 = 4;
	required float					F0 = 5;
}

message TCoefficientsSet {
	optional float 					T0 = 1;
	optional float					C0 = 2;
	optional float					C1 = 3;
	optional float					C2 = 4;
	optional float					F0 = 5;
}

message MeasureResult {
	required uint32					Chanel_number = 1;
	// Статус
	required CHANEL_STATUS			status = 2;
	// Частота [Гц]
	required float 					Freq = 3;
	// Температура [*C]
	required float					Temperature = 4;	
}

//-----------------------------------------------------------------------------

message HistoryReq {
	// Запросить не более MaxSize злементов истории. Или все элементы (-1)
	optional int32					MaxSize = 1 [default = -1];
}

message History {
	// Элементы истории измерений
	repeated HistoryItem			HistoryItems = 1;
}

message HistoryItem {
	required fixed32				Chanel_number = 1;
	// Статус
	required CHANEL_STATUS			status = 2;
	// Частота [Гц]
	required float 					Freq = 3;
	// Температура [*C]
	required float					Temperature = 4;
	// таймштамп
	required fixed64            	Timestamp = 5;
}

//-----------------------------------------------------------------------------

message Mallinfo {
	/* This is the total size of memory allocated
	 * for use by malloc in bytes. */
	required int32 					arena = 1;    
    /* This is the number of free (not in use) chunks */           
  	required int32 					ordblks = 2;
  	/* Size of the largest free (not in use) chunk */
  	required int32 					mxordblk = 3;
  	/* This is the total size of memory occupied by
     * chunks handed out by malloc. */
  	required int32 					uordblks = 4; 
  	/* This is the total size of memory occupied
     * by free (not in use) chunks.*/
  	required int32 					fordblks = 5; 
}
