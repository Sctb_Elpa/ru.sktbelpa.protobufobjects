﻿syntax = "proto2";

package ru.sktbelpa.rkmeter3;

enum INFO {
    // верися этого протокола
    PROTOCOL_VERSION = 2;

    // идентификатор устройства
    RK_METER_3_ID = 0xE110;

    // ИД, преднозначенный для поиска
    ID_DISCOVER = 0xFFFF;

	// магическое число для режима MDProtobuf
    MAGICK = 0x09;
}

// код выполнения операции
enum STATUS {
    // успешное завершение
    OK = 0;

    // недопустимое значение поля
    ERRORS_IN_SUBCOMMANDS = 1;

    PROTOCOL_ERROR = 100;
}

//-----------------------------------------------------------------------------

// Общий запрос для поиска
message Request {
    // Идентификатор запроса. Служит для определения пары запрос/ответ
    required uint32             id = 1;
    // id устройства (идентификация)
    required uint32             deviceID = 2;
    // версия протокола запрашивающего
    required uint32             protocolVersion = 3;

    // запрос выходных значений
    optional OutputReq          getOutputValues = 10;

    // Запрос управления
    optional ControlRequest     control = 20;
}

// Общий ответ сервера
message Response {
    // Идентификатор запроса, которому соответствует этот ответ
    required uint32             id = 1;
    // id устройства
    required uint32             deviceID = 2;
    // версия протокола с которой работает устройство
    required uint32             protocolVersion = 3;
    // общий статус выполнения
    required STATUS             Global_status = 4;
    // таймштамп устройства, когда фактически был сгененрирован ответ
    required fixed64            timestamp = 5;

    // Ответ на запрос выходных значений
    optional OutputResponse     outputValues = 10;

    // Статус управления
    optional ControlResponse    control = 20;
}

//-----------------------------------------------------------------------------

// Запрос выходных значений
message OutputReq {
    // Rk + F + Температура электроники
    optional Empty              getMainValues = 1;

    // отладочные значения
    optional Empty              getRAW = 20;
}

// Выходные значения
message OutputResponse {
    // Rk
    optional float              Rk = 1;

    // Средняя ошибка измерения Rk
    optional float              Error_Rk = 2;

    // F
    optional float              Freq = 3;

    // Результат частотмера
    optional FreqmeterResult    F_result = 20;

    // код АЦП RX
    optional BoxPlot            ADC_RX = 31;

    // код АЦП V_PLL
    optional BoxPlot            ADC_V_PLL = 32;

    // текущий статус PLL
    optional bool               isPllOn = 40;

    // Средний разброс частот за последнюю секунду
    optional float              average_freq_unstable = 50;

    // Состояние цыфровых входов
    optional bool               button = 60;
}

// Результат работы частотомера
message FreqmeterResult {
    required uint32             Target = 1;
    required uint32             Result = 2;
}

//-----------------------------------------------------------------------------

message ControlRequest {
    // Включить режим калибровки
    optional bool               setCalibrate = 1;

    // Включить PLL
    optional PLLState           setPllState = 2;

    // Установить электронный потенциомер в заданное положение
    optional uint32             setPotentiometer = 10;

    // Запись коэффициентов
    optional RkCoefficients     setRkCoefficients = 20;

    // Запрос начала калибровки
    optional bool              calibrationRequest = 30;

    // Установить потенциометр собственной частоты устроства
    optional uint32            setSelfFreqControl = 40;

    // Установить фактическую частоту опорного генератора
    optional uint32            setFref = 50;

    // Смещение нуля Rk
    optional float             setRkZeroOffset = 51;
}

message ControlResponse {
    // Статус калибровки
    required bool               isCalibrate = 1;

    // Статус PLL
    required PLLState           PllState = 2;

    // Положение электронного потенциомера
    required uint32             Potentiometer = 10;

    // Коэффициенты
    required RkCoefficients     RkCoefficients = 20;

    // Статус калибровки
    required bool               isCalibrationInProgress = 30;

    // Потенциометр собственной частоты устроства
    required uint32             SelfFreqControl = 40;

    // Частота опорного генератора
    required uint32             Fref = 50;

    // Смещение нуля Rk
    required float              RkZeroOffset = 51;
}

//-----------------------------------------------------------------------------

// Коэфициенты расчета Rk
message RkCoefficients {
	optional float 				V0 = 1;
	optional float				R0 = 2;
    optional float				A0 = 10;
    optional float				A1 = 11;
    optional float				A2 = 12;
}

// BoxPlot (пришлось выкинуть максимум-минимум. Не влезает в пакет)
message BoxPlot {
    required float              median = 1;
    required float              Q1 = 2;
    required float              Q3 = 3;
    required float              IQR = 4;
    required float              average = 7;
}

enum PLLState {
    OFF = 0;
    ON = 1;
    AUTO = 255;
}

//-----------------------------------------------------------------------------

// пустое сообщение - плейсхолдер
message Empty {}